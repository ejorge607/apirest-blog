'use stric'

const express = require('express')
const ArticleController = require('../controllers/article')

let router = express.Router();
let multipart = require('connect-multiparty')
let mdUpload = multipart({ uploadDir: './upload/articles' });

router.get('/test', ArticleController.test);
router.post('/article', ArticleController.createArticle)
router.get('/articles/:last?', ArticleController.getArticles)
router.get('/article/:articleID', ArticleController.getArticle)
router.put('/article/:articleID', ArticleController.updateArticle)
router.delete('/article/:articleID', ArticleController.deleteArticle)
router.post('/upload-image/:articleID', mdUpload, ArticleController.uploadArticle)
router.get('/get-image/:image', ArticleController.getImage)
router.get('/search/:term', ArticleController.search)



module.exports = router;