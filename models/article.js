'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ArticleSchema = Schema({
    title: { type: String, required: [true, 'The name for create a new article is required'] },
    content: { type: String, required: [true, 'The content is required'] },
    publish: { type: Boolean, default: true, required: true },
    date: { type: Date, default: Date.now() },
    image: { type: String }
})

module.exports = mongoose.model('Article', ArticleSchema);