'use stric'
const validator = require('validator');
const Article = require('../models/article')
const fs = require('fs')
const path = require('path');


const controller = {
    test: (req, res) => {
        return res.status(200).json({
            message: 'Hello World! :)'
        })
    },

    createArticle: (req, res) => {
        let body = req.body;

        let validateTitle = !validator.isEmpty(body.title)
        let validateContent = !validator.isEmpty(body.content)

        if (validateTitle && validateContent) {
            let article = new Article({
                title: body.title,
                content: body.content,
                image: null
            })
            article.save((err, articleDB) => {
                if (err || !articleDB) {
                    return res.status(404).json({
                        result: false,
                        status: 'error',
                        err
                    })
                }
                return res.status(200).json({
                    result: true,
                    status: 'success',
                    article: articleDB
                })
            })
        } else {
            return res.status(400).json({
                result: false,
                message: 'No se pudo completar'
            })
        }
    },

    getArticles: (req, res) => {
        let query = Article.find({ publish: true });

        let last = req.params.last;
        if (last || last != undefined) {
            query.limit(2);
        }

        query.sort('-_id')
            .exec((err, articlesDB) => {
                if (err) {
                    return res.status(500).json({
                        result: false,
                        status: 'error',
                        err
                    })
                }
                if (!articlesDB) {
                    return res.status(404).json({
                        result: false,
                        status: 'error',
                        err
                    })
                }
                res.json({
                    result: true,
                    status: 'success',
                    articlesDB
                })
            })
    },

    getArticle: (req, res) => {
        let articleID = req.params.articleID
        if (!articleID || articleID == null) {
            return res.status(404).json({
                result: false,
                status: 'error',
                message: 'ID Article invalid'
            })
        }
        Article.findById(articleID, (err, articleDB) => {
            if (err) {
                return res.status(500).json({
                    result: false,
                    status: 'error',
                    err
                })
            }
            if (!articleDB) {
                return res.status(404).json({
                    result: false,
                    status: 'error',
                    err
                })
            }
            res.json({
                result: true,
                status: 'Success',
                articleDB
            })

        })
    },

    updateArticle: (req, res) => {
        let articleID = req.params.articleID;
        let body = req.body;
        try {
            var validateTitle = !validator.isEmpty(body.title);
            var validateContent = !validator.isEmpty(body.content);
        } catch (err) {
            return res.status(404).json({
                result: false,
                status: 'Error',
                error: 'Data Uncomplete'
            })
        }
        if (validateTitle && validateContent) {
            Article.findByIdAndUpdate({ _id: articleID }, body, { new: true }, (err, articleDB) => {
                if (err) {
                    return res.status(500).json({
                        result: false,
                        status: 'Error',
                        err
                    })
                }
                if (!articleDB) {
                    return res.status(404).json({
                        result: 'false',
                        status: 'Error',
                        err
                    })
                }
                res.json({
                    result: true,
                    status: 'Success',
                    articleDB
                })
            });
        } else {
            return res.status(404).json({
                result: false,
                status: 'Error',
                message: 'There is a error'
            })
        }
    },

    deleteArticle: (req, res) => {
        let articleID = req.params.articleID;
        let changePublish = {
            publish: false
        }
        Article.findByIdAndUpdate(articleID, changePublish, { new: true }, (err, articleDeleted) => {
            if (err) {
                return res.status(500).json({
                    result: false,
                    status: 'Error',
                    err
                })
            }
            if (!articleDeleted) {
                return res.status(404).json({
                    result: 'false',
                    status: 'Error',
                    err
                })
            }
            res.json({
                result: true,
                status: 'Success',
                articleDeleted
            })
        })
    },

    uploadArticle: (req, res) => {
        let fileImage = 'Image not upload';
        let articleID = req.params.articleID;
        if (!req.files) {
            return res.status(404).json({
                result: false,
                status: 'Error',
                message: 'Image not received'
            })
        }

        let file_path = req.files.file0.path;
        let file_split = file_path.split('/');

        let file_name = file_split[2];
        let split_extension = file_name.split('.');
        let file_extension = split_extension[split_extension.length - 1];
        //let file_extension = split_extension[1]
        let validExtensions = ['png', 'jpg', 'gif', 'jpeg'];

        if (validExtensions.indexOf(file_extension) < 0) {
            fs.unlink(file_path, (err) => {
                return res.status(200).json({
                    result: false,
                    status: 'Error',
                    Error: `The extension ${file_extension} is not valid.`,
                    validExtensions: validExtensions.join(', ')
                })
            })
        } else {
            Article.findByIdAndUpdate(articleID, { image: file_name }, { new: true }, (err, articleUpdated) => {
                if (err) {
                    return res.status(500).json({
                        result: false,
                        status: 'Error',
                        err
                    })
                }
                if (!articleUpdated) {
                    return res.status(404).json({
                        result: false,
                        status: 'Error',
                        err
                    })
                }
                res.json({
                    result: true,
                    status: 'Success',
                    articleUpdated
                })
            })
        }
    },

    getImage: (req, res) => {
        let file = req.params.image;
        let pathUrlImage = path.resolve(__dirname, `../upload/articles/${file}`);
        fs.exists(pathUrlImage, (exist) => {
            if (exist) {
                return res.sendFile(pathUrlImage)
            } else {
                return res.status(404).json({
                    result: false,
                    status: 'Error',
                    message: `File ${file} not Found`
                })
            }
        })
    },

    search: (req, res) => {
        let term = req.params.term;
        Article.find({
            "$or": [
                { "title": { "$regex": term, "$options": "i" } },
                { "content": { "$regex": term, "$options": "i" } }
            ]
        }).sort([
            ['date', 'descending']
        ]).exec((err, result) => {
            if (err) {
                return res.status(500).json({
                    result: false,
                    status: 'Error',
                    err
                })
            }
            if (!result || result.length <= 0) {
                return res.status(404).json({
                    result: false,
                    status: 'Error',
                    message: 'Not match with any article'
                })
            }
            res.json({
                ok: true,
                status: 'Success',
                result
            })
        })
    }

}

module.exports = controller;