'use strict'
const mongoose = require('mongoose')
let app = require('./app')
let port = 3900;

mongoose.set('useFindAndModify', false)
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/api_rest_blog', { useNewUrlParser: true })
    .then(() => {
        console.log('Connected to Mongo DB');
        app.listen(port, () => {
            console.log(`Server running on the port ${port}`);
        })
    })