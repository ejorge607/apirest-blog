![Everardo Jorge](https://gitlab.com/ejorge607/apirest-blog/-/raw/master/public/img/API-REST.png)
# **Simple API Rest**
Este es un proyecto creado con fines practicos utilizando tecnologias como **NodeJS, Express, MongoDB**, el cual posee 2 objetivos.
## 1. Estructura de API REST (Backend).
**Ver**, **analizar** y **comprender** el como es la construcción o estructura de una **simple API Rest**. El proyecto contiene todos los archivos necesarios para funcionar.

## 2. Consumir Datos (Frontend).
Si lo que buscas es crear una interfaz grafica utilizando un framework, esta API Rest te sera de mucha ayuda. Ya que podras ejecutarla, y empezar a utilizarla.

### Requisitos
* Tener instalado NodeJS
* Tener instalado MongoDB
* Tener instalado Git
* Rest Client
  * Insomnia.
  * Postman.
  * o el de su preferencia.


### Ejecutar código
1. Descargar o clonar el repositorio.
  * Para descarlo pulsar el botón de ***Descargar*** (Download).
  * Para clonarlo, abre un terminal o cmd y ejecutar el comando: ***git clone https://gitlab.com/ejorge607/apirest-blog.git***.
2. Una vez descargado, moverse a la carpeta con el proyecto y ejecutar el siguiente comando: ***npm install***, lo que hace este comando es instalar las dependencias necesarias.
3. Terminado el paso anterior, encender mongodb en su ordenador.
4. Para arrancar el API Rest, ejecute el comando: ***node index.js***.


### Como funciona
#### Crear un articulo
La url para crar un árticulo es: ***localhost:3900/api/article***
y le pasamos por el *Form Url Encode* los parametos de ***title y content*** con su respectivo valor, y eso es una petición **POST**
>Ejemplo de respuesta
```json
{
  "result": true,
  "status": "success",
  "article": {
    "publish": true,
    "date": "2020-02-25T16:24:23.232Z",
    "_id": "5e554acc24398614970eddf0",
    "title": "Ejemplo 1",
    "content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "image": null,
    "__v": 0
  }
}
```

#### Obtener todos los árticulos
Enviar una petición **GET** a la siguiente url ***localhost:3900/api/articles***
>Ejemplo de respuesta:
```json
{
  "result": true,
  "status": "success",
  "articlesDB": [{
      "publish": true,
      "date": "2020-02-25T16:24:23.232Z",
      "_id": "5e554acc24398614970eddf0",
      "title": "Ejemplo 1",
      "content": "ejemplo 1",
      "image": "jwCOHvmtNVyaTeyjLIoY0WMU.png",
      "__v": 0
    },{
        "publish": true,
        "date": "2020-02-25T16:24:23.232Z",
        "_id": "5e554acc24398614970eddf0",
        "title": "Ejemplo 1",
        "content": "ejemplo 1",
        "image": "jwCOHvmtNVyaTeyjLIoY0WMU.png",
        "__v": 0
      }]}
```

#### Obtener un solo árticulo
Enviar una preticón **GET** a la url ***localhost:3900/api/article/ID_articulo***
>Ejemplo de respuesta
```json
{
  "result": true,
  "status": "Success",
  "articleDB": {
    "date": "2020-02-25T05:33:50.329Z",
    "_id": "5e54b1e4c0b4301da664cfa6",
    "title": "Ejemplo 1",
    "content": "Ejemplo 1",
    "image": null,
    "__v": 0
  }
}
```

#### Actualizar un árticulo
Enviar una peticion **PUT** a la url ***localhost:3900/api/article/ID_articulo*** y pasarle por el *From Url Encode* los parametros a actualizar, ejemplo el **title o content**
>Ejemplo de respuesta
```json
{
  "result": true,
  "status": "Success",
  "articleDB": {
    "publish": true,
    "date": "2020-02-25T16:24:23.232Z",
    "_id": "5e554a7424398614970eddee",
    "title": "Ejemplo Actualizado",
    "content": "Ejemplo 1",
    "image": null,
    "__v": 0
  }
}
```
#### Eliminar un árticulo
**Nota:** En esta API REST los árticulos no se eliminan, solo cambia el valor de **publish** de **true** a **false**, para que ya no esten disponibles.
Para eliminar un árticulo enviar una petición ***DELETE*** a la siguiente url ***localhost:3900/api/article/ID_articulo***
>Ejemplo de respuesta
```json
{
  "result": true,
  "status": "Success",
  "articleDeleted": {
    "publish": false,
    "date": "2020-02-25T16:24:23.232Z",
    "_id": "5e554a7424398614970eddee",
    "title": "Ejemplo 2",
    "content": "Ejemplo 2",
    "image": null,
    "__v": 0
  }
}
```
